package com.vti.shoppera59backend.exception;

import org.springframework.http.HttpStatus;

public enum ErrorResponseBase {
    NOT_FOUND(HttpStatus.NOT_FOUND, "Đối tượng ko tồn tại"),
    USERNAME_EXISTED(HttpStatus.INTERNAL_SERVER_ERROR, "Username đã tồn tại"),
    NOT_FOUND2(HttpStatus.NOT_FOUND, "Đối tượng ko tồn tại"),
    MIN_MAX_INVALID(HttpStatus.BAD_REQUEST, "Số min phải nhỏ hơn max"),
    LOGIN_FAILS(HttpStatus.UNAUTHORIZED, "Người dùng không tồn tại"),
    ;

    public final HttpStatus status;
    public final String message;

    ErrorResponseBase(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
