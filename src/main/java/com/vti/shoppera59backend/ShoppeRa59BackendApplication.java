package com.vti.shoppera59backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShoppeRa59BackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShoppeRa59BackendApplication.class, args);
    }

}
