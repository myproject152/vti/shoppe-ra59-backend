package com.vti.shoppera59backend.repository.specification;

import com.vti.shoppera59backend.modal.entity.Product;
import com.vti.shoppera59backend.modal.entity.ProductStatus;
import com.vti.shoppera59backend.modal.entity.ProductType;
import com.vti.shoppera59backend.modal.entity.ShippingUnit;
import com.vti.shoppera59backend.modal.request.SearchProductRequest;
import org.springframework.data.jpa.domain.Specification;

public class ProductSpecification {

    public static Specification<Product> buildCondition(SearchProductRequest request){
        return Specification.where(byProductName(request.getProductName()))
                .and(byProductType(request.getProductType()))
                .and(byShippingUnit(request.getShippingUnit()))
                .and(byProductStatus(request.getProductStatus()))
                .and(byPriceMin(request.getMinPrice()))
                .and(byPriceMax(request.getMaxPrice()));
    }

    private static Specification<Product> byProductName(String productName) {
        if (productName != null) {
            return (root, query, cri) -> {
                return cri.like(root.get("name"), "%" + productName + "%");
            };
        }
        return null;
    }

    private static Specification<Product> byProductType(ProductType productType) {
        if (productType != null) {
            return (root, query, cri) -> {
                return cri.equal(root.get("type"), productType);
            };
        }
        return null;
    }

    private static Specification<Product> byShippingUnit(ShippingUnit shippingUnit) {
        if (shippingUnit != null) {
            return (root, query, cri) -> {
                return cri.equal(root.get("shippingUnit"), shippingUnit);
            };
        }
        return null;
    }

    private static Specification<Product> byProductStatus(ProductStatus productStatus) {
        if (productStatus != null) {
            return (root, query, cri) -> {
                return cri.equal(root.get("status"), productStatus);
            };
        }
        return null;
    }

    private static Specification<Product> byPriceMin(Long minPrice) {
        if (minPrice != null) {
            return (root, query, cri) -> {
                return cri.greaterThanOrEqualTo(root.get("price"), minPrice);
            };
        }
        return null;
    }

    private static Specification<Product> byPriceMax(Long maxPrice) {
        if (maxPrice != null) {
            return (root, query, cri) -> {
                return cri.lessThanOrEqualTo(root.get("price"), maxPrice);
            };
        }
        return null;
    }
}
