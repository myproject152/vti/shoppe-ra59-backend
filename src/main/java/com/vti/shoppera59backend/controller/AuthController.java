package com.vti.shoppera59backend.controller;

import com.vti.shoppera59backend.exception.AppException;
import com.vti.shoppera59backend.exception.ErrorResponseBase;
import com.vti.shoppera59backend.modal.dto.LoginDto;
import com.vti.shoppera59backend.modal.entity.Account;
import com.vti.shoppera59backend.repository.AccountRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/auth")
@CrossOrigin("*")
@Validated
public class AuthController {

    @Autowired
    private AccountRepository accountRepository;

    @PostMapping("/login")
    public LoginDto login(Principal principal) { // đối tượng Principal có chứa thông tin username
        // Khi qua bước authen sẽ tạo ra đối tượng Principal, tại controller sẽ sử dụng lại giá trị của dữ liệu này
        String username = principal.getName();
        Optional<Account> accountOptional = accountRepository.findByUsername(username);
        if (accountOptional.isEmpty()) {
            throw new AppException(ErrorResponseBase.LOGIN_FAILS);
        }
        LoginDto loginDto = new LoginDto();
        BeanUtils.copyProperties(accountOptional.get(), loginDto);
        return loginDto;
    }
}