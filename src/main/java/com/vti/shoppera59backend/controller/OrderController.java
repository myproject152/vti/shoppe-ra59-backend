package com.vti.shoppera59backend.controller;

import com.vti.shoppera59backend.modal.entity.Order;
import com.vti.shoppera59backend.modal.request.CreatOrderRequest;
import com.vti.shoppera59backend.modal.request.SearchOrderRequest;
import com.vti.shoppera59backend.service.impl.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/order")
@CrossOrigin("*")
@Validated
public class OrderController {
    @Autowired
    private OrderService service;

    @GetMapping("/get-all")
    public List<Order> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Order getById(@PathVariable int id) {
        return service.getById(id);
    }

    @PostMapping("/search")
    public Page<Order> create(@RequestBody SearchOrderRequest request) {
        return service.search(request);
    }

    @PostMapping("/create")
    public void create(@RequestBody @Valid CreatOrderRequest request) {
            service.create(request);
    }

    @PutMapping("/update/{id}")
    public Order update(@RequestBody CreatOrderRequest request, @PathVariable int id) {
        return service.update(id, request);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        service.delete(id);
    }
}
