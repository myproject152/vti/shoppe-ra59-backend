package com.vti.shoppera59backend.controller;

import com.vti.shoppera59backend.modal.entity.Product;
import com.vti.shoppera59backend.modal.request.CreatProductRequest;
import com.vti.shoppera59backend.modal.request.SearchProductRequest;
import com.vti.shoppera59backend.service.impl.ProductService;
import com.vti.shoppera59backend.validate.ProductIDExists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/product")
@CrossOrigin("*")
@Validated
public class ProductController {

    @Autowired
    private ProductService service;

    @GetMapping("/get-all")
    public List<Product> getAll() {
        return service.getAll();
    }

    @PostMapping("/search")
    public Page<Product> search(@RequestBody SearchProductRequest request) {
        return service.search(request);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public Product getById(@PathVariable @ProductIDExists int id) {
        return service.getById(id);
    }

    @PostMapping("/create")
    public void create(@RequestBody @Valid CreatProductRequest request) {
        service.create(request);
    }

    @PutMapping("/update/{id}")
    public Product update(@RequestBody @Valid CreatProductRequest request, @PathVariable int id) {
        return service.update(id, request);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        service.delete(id);
    }
}
