package com.vti.shoppera59backend.validate;

import com.vti.shoppera59backend.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

//GHI CHÚ: ConstraintValidator< Annotation đã tạo, kiểu dữ liệu muốn kiểm tra>
public class ProductIDExistsValidator implements ConstraintValidator<ProductIDExists, Integer> {
    @Autowired
    ProductRepository repository;

    @Override
    public boolean isValid(Integer integer, ConstraintValidatorContext constraintValidatorContext) {
        // Logic kiểm tra dữ liệu
        return repository.existsById(integer);
    }
}
