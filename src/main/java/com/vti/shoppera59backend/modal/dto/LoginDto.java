package com.vti.shoppera59backend.modal.dto;

import com.vti.shoppera59backend.modal.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginDto {
    private int id;
    private String username;
    private Role role;
    private String fullName;
}
