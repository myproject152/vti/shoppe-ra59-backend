package com.vti.shoppera59backend.modal.request;

import com.vti.shoppera59backend.modal.entity.ProductStatus;
import com.vti.shoppera59backend.modal.entity.ProductType;
import com.vti.shoppera59backend.modal.entity.ShippingUnit;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CreatProductRequest {
    @NotBlank(message = "{product.create.name}")
    private String name;

    @NotBlank(message = "{product.test}")
    private String  image;// ảnh đại diện sản phẩm

    private int price;

    private ProductStatus status;// tình trạng sản phẩm mới hay cũ

    private ShippingUnit shippingUnit;//đơn vị nhận vận chuyển

    private ProductType type;//loại sản phẩm bán
}
