package com.vti.shoppera59backend.modal.request;

import com.vti.shoppera59backend.modal.entity.Role;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.*;
import java.util.Date;

@Data
public class CreateAccountRequest {
    @NotBlank(message = "Tên ko được để trống")
    private String username;

    private Role role;

    @NotBlank(message = "password ko được để trống")
    @Size(min = 3, max = 6, message = "pw phải có 3-6 ký tự")
    private String password;

    private Date dateOfBirth;

    private String address;

    private String fullName;

//    @Pattern(regexp = "/(84[3|5|7|8|9])+([0-9]{8})\\b/g", message = "Số điện thoại ko đúng định dạng")
    private String phoneNumber;

    private String email;

    private String facebook;

    private String information;

//    @Min(value = 3, message = "Số phải lớp hơn 3")
//    @Max(value = 3, message = "Số phải lớp hơn 3")
//    private int abc;
}
