package com.vti.shoppera59backend.modal.request;

import com.vti.shoppera59backend.modal.dto.BaseRequest;
import com.vti.shoppera59backend.modal.entity.ProductStatus;
import com.vti.shoppera59backend.modal.entity.ProductType;
import com.vti.shoppera59backend.modal.entity.ShippingUnit;
import com.vti.shoppera59backend.modal.entity.StatusOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchOrderRequest extends BaseRequest {
    private int orderBy;
    private StatusOrder statusOrder;
}
