package com.vti.shoppera59backend.modal.request;

import com.vti.shoppera59backend.modal.dto.BaseRequest;
import com.vti.shoppera59backend.modal.entity.ProductStatus;
import com.vti.shoppera59backend.modal.entity.ProductType;
import com.vti.shoppera59backend.modal.entity.ShippingUnit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchProductRequest extends BaseRequest {
    private String productName;

    private ProductType productType;

    private ShippingUnit shippingUnit;

    private ProductStatus productStatus;

    private Long minPrice;

    private Long maxPrice;
}
