package com.vti.shoppera59backend.modal.request;

import com.vti.shoppera59backend.modal.entity.StatusOrder;
import com.vti.shoppera59backend.validate.ProductIDExists;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
public class CreatOrderRequest {
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date orderDate;// ngày order

    private int accountId;//người order

    @ProductIDExists()
    private int productId;

    private int quantity;//số lượng sản phẩm đã order

    private StatusOrder status;
}
