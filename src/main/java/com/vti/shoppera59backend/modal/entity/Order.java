package com.vti.shoppera59backend.modal.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "`Order`")
public class Order {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "order_date")
    private Date orderDate;// ngày order

    @ManyToOne()
    @JoinColumn(name = "order_by")
    private Account orderBy;//người order

    @ManyToOne()
    @JoinColumn(name = "product_id")
    private Product product;

    @Column(name = "quantity")
    private int quantity;//số lượng sản phẩm đã order

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private StatusOrder status;
}