package com.vti.shoppera59backend.service.impl;

import com.vti.shoppera59backend.exception.AppException;
import com.vti.shoppera59backend.exception.ErrorResponseBase;
import com.vti.shoppera59backend.modal.entity.Account;
import com.vti.shoppera59backend.modal.request.CreateAccountRequest;
import com.vti.shoppera59backend.modal.request.UpdateAccountRequest;
import com.vti.shoppera59backend.repository.AccountRepository;
import com.vti.shoppera59backend.service.IAccountService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class AccountService implements IAccountService, UserDetailsService {
    @Autowired
    private AccountRepository repository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Account> optional = repository.findByUsername(username);
        if (optional.isPresent()){
            Account account = optional.get();
            // Lấy giá trị authorities để phân quyền
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(account.getRole());

            return new User(account.getUsername(), account.getPassword(), authorities);
        } else {
            throw new UsernameNotFoundException(username);
        }
    }

    @Override
    public List<Account> getAll() {
        return repository.findAll();
    }

    @Override
    public Account getById(int id) {
        Optional<Account> optional = repository.findById(id);
        if (optional.isEmpty()){
            throw new AppException(ErrorResponseBase.NOT_FOUND);
        }
            return optional.get();

    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void create(CreateAccountRequest request) {
        Account account = new Account();
        String encodePassword = encoder.encode(request.getPassword());
        BeanUtils.copyProperties(request, account);
        account.setPassword(encodePassword);
        // Kiểm tra username đã tồn tại chưa
        Optional<Account> accountCheck = repository.findByUsername(request.getUsername());
        if (accountCheck.isPresent()){
            // username đã tồn tại -> bắn lỗi
            throw new AppException(ErrorResponseBase.USERNAME_EXISTED);
        }
            repository.save(account);

    }

    @Override
    public Account update(int id, UpdateAccountRequest request) {
        Account accountDb = getById(id);
        if (accountDb != null){
            BeanUtils.copyProperties(request, accountDb);
            accountDb.setId(id);
            return repository.save(accountDb);
        }
        return null;
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }
}
