package com.vti.shoppera59backend.service.impl;

import com.vti.shoppera59backend.exception.AppException;
import com.vti.shoppera59backend.exception.ErrorResponseBase;
import com.vti.shoppera59backend.modal.dto.BaseRequest;
import com.vti.shoppera59backend.modal.entity.Product;
import com.vti.shoppera59backend.modal.request.CreatProductRequest;
import com.vti.shoppera59backend.modal.request.SearchProductRequest;
import com.vti.shoppera59backend.repository.ProductRepository;
import com.vti.shoppera59backend.repository.specification.ProductSpecification;
import com.vti.shoppera59backend.service.IProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService implements IProductService {
    @Autowired
    private ProductRepository repository;

    @Override
    public List<Product> getAll() {
        return repository.findAll();
    }

    @Override
    public Page<Product> search(SearchProductRequest request) {
        long minPrice = request.getMinPrice();
        long maxPrice = request.getMaxPrice();
        if (minPrice >= maxPrice){
            throw new AppException(ErrorResponseBase.MIN_MAX_INVALID);
        }

        Specification<Product> condition = ProductSpecification.buildCondition(request);
        PageRequest pageRequest = BaseRequest.buildPageRequest(request);

        return repository.findAll(condition, pageRequest);
    }

    @Override
    public Product getById(int id) {
        Optional<Product> optional = repository.findById(id);
        if (optional.isPresent()){
            return optional.get();
        }
        return null;
    }

    @Override
    public void create(CreatProductRequest request) {
        Product product = new Product();
        BeanUtils.copyProperties(request, product);
        repository.save(product);
    }

    @Override
    public Product update(int id, CreatProductRequest request) {
        Product product = getById(id);
        if (product != null){
            BeanUtils.copyProperties(request, product);
            product.setId(id);
            return repository.save(product);
        }
        return null;
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }
}
