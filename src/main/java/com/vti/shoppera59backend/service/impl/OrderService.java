package com.vti.shoppera59backend.service.impl;

import com.vti.shoppera59backend.modal.dto.BaseRequest;
import com.vti.shoppera59backend.modal.entity.Account;
import com.vti.shoppera59backend.modal.entity.Order;
import com.vti.shoppera59backend.modal.entity.Product;
import com.vti.shoppera59backend.modal.entity.StatusOrder;
import com.vti.shoppera59backend.modal.request.CreatOrderRequest;
import com.vti.shoppera59backend.modal.request.SearchOrderRequest;
import com.vti.shoppera59backend.repository.OrderRepository;
import com.vti.shoppera59backend.repository.specification.OrderSpecification;
import com.vti.shoppera59backend.repository.specification.ProductSpecification;
import com.vti.shoppera59backend.service.IOderService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService implements IOderService {
    @Autowired
    private OrderRepository repository;

    @Autowired
    ProductService productService;

    @Autowired
    AccountService accountService;

    @Override
    public List<Order> getAll() {
        return repository.findAll();
    }

    @Override
    public Page<Order> search(SearchOrderRequest request) {
        Specification<Order> condition = OrderSpecification.buildCondition(request);
        PageRequest pageRequest = BaseRequest.buildPageRequest(request);
        return repository.findAll(condition, pageRequest);
    }

    @Override
    public Order getById(int id) {
        Optional<Order> optional = repository.findById(id);
        if (optional.isPresent()){
            return optional.get();
        }
        return null;
    }

    @Override
    public void create(CreatOrderRequest request) {
        Order order = new Order();
        BeanUtils.copyProperties(request, order);
        // Get 2 đối tượng Account và Product từ các service tương ứng
        Account account = accountService.getById(request.getAccountId());
        Product product = productService.getById(request.getProductId());
        if (account != null && product != null){
            // Setup creatBy và product notnull
            order.setOrderBy(account);
            order.setProduct(product);
            order.setOrderDate(new Date());
            order.setStatus(StatusOrder.PENDING);
            repository.save(order);
        }
    }

    @Override
    public Order update(int id, CreatOrderRequest request) {
        Order order = getById(id);
        if (order != null){
            BeanUtils.copyProperties(request, order);
            order.setId(id);
            return repository.save(order);
        }
        return null;
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }
}
