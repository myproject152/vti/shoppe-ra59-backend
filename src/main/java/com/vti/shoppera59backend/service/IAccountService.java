package com.vti.shoppera59backend.service;

import com.vti.shoppera59backend.modal.entity.Account;
import com.vti.shoppera59backend.modal.request.CreateAccountRequest;
import com.vti.shoppera59backend.modal.request.UpdateAccountRequest;

import java.util.List;

public interface IAccountService {
    List<Account> getAll();

    Account getById(int id);

    void create(CreateAccountRequest request);

    Account update(int id, UpdateAccountRequest request);

    void delete(int id);
}
