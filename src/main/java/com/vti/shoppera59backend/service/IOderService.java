package com.vti.shoppera59backend.service;

import com.vti.shoppera59backend.modal.entity.Order;
import com.vti.shoppera59backend.modal.request.CreatOrderRequest;
import com.vti.shoppera59backend.modal.request.SearchOrderRequest;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IOderService {
    List<Order> getAll();

    Page<Order> search(SearchOrderRequest request);

    Order getById(int id);

    void create(CreatOrderRequest request);

    Order update(int id, CreatOrderRequest request);

    void delete(int id);
}
