package com.vti.shoppera59backend.service;

import com.vti.shoppera59backend.modal.entity.Product;
import com.vti.shoppera59backend.modal.request.CreatProductRequest;
import com.vti.shoppera59backend.modal.request.SearchProductRequest;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IProductService {
    List<Product> getAll();

    Page<Product> search(SearchProductRequest request);

    Product getById(int id);

    void create(CreatProductRequest request);

    Product update(int id, CreatProductRequest request);

    void delete(int id);
}
